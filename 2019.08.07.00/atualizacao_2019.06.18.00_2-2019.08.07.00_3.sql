set term ^;
CREATE or alter PROCEDURE PRC_EXPORTAPRODUTOINDIVIDUALPDV (
    in_codempresa integer,
    in_codproduto integer)
returns (
    out_codigobarras varchar(20),
    out_descricaoreduzida varchar(50),
    out_peso numeric(1,0),
    out_estoqueminimo numeric(15,4),
    out_estoquemaximo numeric(15,4),
    out_unidade varchar(3),
    out_codfornecedor numeric(1,0),
    out_precovenda numeric(15,4),
    out_stpdv numeric(2,0),
    out_aliquotaicms numeric(15,4),
    out_path_foto varchar(100),
    out_margemobj numeric(15,4),
    out_customedio numeric(15,4),
    out_custounitario numeric(15,4),
    out_dataexportacao date,
    out_desscricaoreduzida2 varchar(20),
    out_retornavel char(1),
    out_datainiciopromocao date,
    out_datafimpromocao date,
    out_codpromocao numeric(9,0),
    out_precopromocional numeric(15,4),
    out_tipounidade char(1),
    out_teclaassociadabalanca char(4),
    out_diasvalidade numeric(4,0),
    out_codproduto numeric(9,0),
    out_codgrupo numeric(9,0),
    out_situacaoproduto char(1),
    out_incluirbuscarapida char(1),
    out_ippt char(1),
    out_estoqueatual numeric(18,4),
    out_percentualbaseicms numeric(15,4),
    out_cstb char(2),
    out_aliqnac numeric(15,4),
    out_aliqimp numeric(15,4),
    out_percentualbcpis float,
    out_percentualbccofins float,
    out_csta char(1),
    out_percentualpis numeric(15,4),
    out_percentualcofins numeric(15,4),
    out_codncm varchar(8),
    out_aliqest numeric(15,4),
    out_aliqmun numeric(15,4),
    out_aliquotapis numeric(15,4),
    out_aliquotacofins numeric(15,4),
    out_cstpis varchar(2),
    out_cstcofins varchar(2),
    out_csosn char(3),
    out_cest char(7),
    out_pst numeric(15,4),
    out_vbcret numeric(15,4),
    out_vicmsstret numeric(15,4),
    out_percredbcefet numeric(15,4),
    out_aliquotaicmsst numeric(15,4),
    out_percentualfcp numeric(15,4),
    out_tabelaadicional varchar(10),
    out_percicmsdesonerado numeric(15,4))
as
BEGIN

    /*==============================================================*/
    /*== seleciona os dados do produto em promo??o                ==*/
    /*==============================================================*/
    FOR SELECT codigobarras.codigobarras,
               SUBSTRING(produto.descricao FROM 1 FOR 50),
               0,
               valoresproduto.estoqueminimo,
               valoresproduto.estoquemaximo,
               SUBSTRING(valoresproduto.unidademercadoria FROM 1 FOR 3), --valoresproduto.unidademercadoria,--produto.unidade,
               0,
               valoresproduto.precovenda,
               produto.situacaotributariatpdv,
               produto.aliquotaicms,
               COALESCE(produto.path_foto,' ') AS path_foto,
               0.0,
               0.0,
               --0.0,retirado para passar o fatordeconversao dos produtos
               produto.fatordeconversao,--modifica??o feita por giucimar dias vieira
               CURRENT_DATE,
               produto.descricaoreduzida,
               produto.retornavel,
               promocao.datainicio,
               promocao.datafim,
               produtopromocao.precopromocional,
               promocao.codpromocao,
               produto.tipounidade,
               valoresproduto.situacaoproduto,--
               produto.teclaassociadabalanca,
               produto.diasvalidade,
               produto.codproduto,
               subgrupo.codgrupo,
               produto.incluirbuscarapida, --
               produto.ippt,
               estoque.estoquefinal,
               produto.percentualbaseicms,
               produto.percentualbcpis,
               produto.percentualbccofins,
               COALESCE(tributacaoibpt.aliqnac,0),
               COALESCE(tributacaoibpt.aliqimp,0),
               produto.cst_b,
               produto.codproduto,
               produto.cst_a,
               produto.codncm,
               COALESCE(tributacaoibpt.aliqestadual,0),
               COALESCE(tributacaoibpt.aliqmunicipal,0),
               COALESCE(produto.aliquotapis,0),
               COALESCE(produto.aliquotacofins,0),
               produto.cstpis,
               produto.cstcofins,
               COALESCE(produto.csosn,''),
               COALESCE(produto.cest,''),
               Coalesce(valoresproduto.pst,0),
               Coalesce(valoresproduto.vbcret,0),
               Coalesce(valoresproduto.vicmsstret,0),
               Coalesce(produto.PERCREDBCST,0),
               Coalesce(produto.ALIQUOTAICMSST,0),
               coalesce(produto.percentualfcp,0),
               case  when ((produto.cst_b  = '00' and tabadicionalcodinf.cst_b_00 = 'S') or
               (produto.cst_b  = '10' and tabadicionalcodinf.cst_b_10 = 'S') or
               (produto.cst_b  = '20' and tabadicionalcodinf.cst_b_20 = 'S') or
               (produto.cst_b  = '30' and tabadicionalcodinf.cst_b_30 = 'S') or
               (produto.cst_b  = '40' and tabadicionalcodinf.cst_b_40 = 'S') or
               (produto.cst_b  = '41' and tabadicionalcodinf.cst_b_41 = 'S') or
               (produto.cst_b  = '50' and tabadicionalcodinf.cst_b_50 = 'S') or
               (produto.cst_b  = '51' and tabadicionalcodinf.cst_b_51 = 'S') or
               (produto.cst_b  = '60' and tabadicionalcodinf.cst_b_60 = 'S') or
               (produto.cst_b  = '70' and tabadicionalcodinf.cst_b_70 = 'S') or
               (produto.cst_b  = '90' and tabadicionalcodinf.cst_b_90 = 'S')
                )  then  tabadicionalcodinf.uf||tabadicionalcodinf.codinfadicional
                else configuracoes.cbenef end ,
                coalesce(produto.percicmsdesonerado,0)
        FROM   codigobarras,
               produto LEFT JOIN tributacaoibpt ON (produto.codtributacaoibpt = tributacaoibpt.codtributacaoibpt)
                       left join tabadicionalcodinf on tabadicionalcodinf .codtabadicional = produto.codtabelaadicional
                       and  produto.tipotabelaadiconal = tabadicionalcodinf.tipo,
               valoresproduto,
               promocao,
               produtopromocao,
               empresapromocao,
               subgrupo,
               estoque,
               ncm_validacao,
               configuracoes
        WHERE  produto.codproduto               = :in_codproduto
        and    configuracoes.codempresa            = estoque.codempresa
        and    configuracoes.codempresa            = valoresproduto.codempresa
        AND    estoque.codempresa               = :in_codempresa
        AND    estoque.anoestoque               = EXTRACT(YEAR FROM CURRENT_DATE)
        AND    estoque.mesestoque               = EXTRACT(MONTH FROM CURRENT_DATE)
        AND    CURRENT_DATE                     BETWEEN promocao.datainicio AND promocao.datafim
        AND    produtopromocao.precopromocional > 0
        AND    produto.codsubgrupo              = subgrupo.codsubgrupo
        AND    produto.codproduto               = estoque.codproduto
        AND    estoque.codproduto               = valoresproduto.codproduto
        AND    estoque.codempresa               = valoresproduto.codempresa
        AND    valoresproduto.codproduto        = codigobarras.codproduto
        AND    valoresproduto.codigobarras      = codigobarras.codigobarras    -- Ajuste p/requisito #1463
        AND    estoque.codproduto               = produtopromocao.codproduto
        AND    estoque.codempresa               = empresapromocao.codempresa
        AND    produtopromocao.codpromocao      = empresapromocao.codpromocao
        AND    empresapromocao.codpromocao      = promocao.codpromocao
        AND    produto.codncm                      = ncm_validacao.codncm
        AND    ((produto.codtipoitem = 0) OR (produto.codtipoitem = 4) OR (produto.codtipoitem = 9))-- alterado por vin?cius no dia 19/04/2010

       /* AND    (produtopromocao.codpromocao||produtopromocao.codproduto) IN
                                (SELECT DISTINCT MAX(produtopromocao.codpromocao)||produtopromocao.codproduto
                                 FROM  promocao,
                                       produtopromocao,
                                       empresapromocao
                                 WHERE CURRENT_DATE               BETWEEN promocao.datainicio AND promocao.datafim
                                 AND   promocao.codpromocao       = produtopromocao.codpromocao
                                 AND   promocao.codpromocao       = empresapromocao.codpromocao
                                 AND   empresapromocao.codempresa = :in_codempresa
                                 AND   promocao.codpromocao       > 0
                                 GROUP BY produtopromocao.codproduto)    */
    
        INTO   :out_codigobarras,
               :out_descricaoreduzida,
               :out_peso,
               :out_estoqueminimo,
               :out_estoquemaximo,
               :out_unidade,
               :out_codfornecedor,
               :out_precovenda,
               :out_stpdv,
               :out_aliquotaicms,
               :out_path_foto,
               :out_margemobj,
               :out_customedio,
               :out_custounitario,
               :out_dataexportacao,
               :out_desscricaoreduzida2,
               :out_retornavel,
               :out_datainiciopromocao,
               :out_datafimpromocao,
               :out_precopromocional,
               :out_codpromocao,
               :out_tipounidade,
               :out_situacaoproduto, --
               :out_teclaassociadabalanca,
               :out_diasvalidade,
               :out_codproduto,
               :out_codgrupo,
               :out_incluirbuscarapida,--
               :out_ippt,
               :out_estoqueatual,
               :out_percentualbaseicms,
               :out_percentualbcpis,
               :out_percentualbccofins,
               :out_aliqnac,
               :out_aliqimp,
               :out_cstb,
               :out_codproduto,
               :out_csta,
               :out_codncm,
               :out_aliqest,
               :out_aliqmun,
               :out_percentualpis,
               :out_percentualcofins,
               :out_cstpis,
               :out_cstcofins,
               :out_csosn,
               :out_cest,
               :out_pst,
               :out_vbcret,
               :out_vIcmsSTRet,
               :out_PercRedBCEfet,
               :out_ALIQUOTAICMSST,
               :out_percentualfcp,
               :out_tabelaadicional,
               :out_percicmsdesonerado

    DO
    BEGIN
        IF (:out_codproduto IS NOT NULL) THEN
        BEGIN
            SUSPEND;
        END
    END
    /*===============================================================*/
    /*== se o produto n?o foi encontrado na promo??o seleciona os  ==*/
    /*== dados normais do produto                                  ==*/
    /*===============================================================*/
    IF (:out_codproduto IS NULL) THEN /* out_codproduto */
    BEGIN
       FOR SELECT DISTINCT
                  codigobarras.codigobarras,
                  SUBSTRING(produto.descricao FROM 1 FOR 50),
                  0,
                  valoresproduto.estoqueminimo,
                  valoresproduto.estoquemaximo,
                  SUBSTRING(valoresproduto.unidademercadoria FROM 1 FOR 3), --valoresproduto.unidademercadoria,--produto.unidade,
                  0,
                  valoresproduto.precovenda,
                  produto.situacaotributariatpdv,
                  produto.aliquotaicms,
                  COALESCE(produto.path_foto,' '),
                  0.0,
                  0.0,
                  0.0,
                  CURRENT_DATE,
                  produto.descricaoreduzida,
                  produto.retornavel,
                  CAST('01/01/1980' AS DATE),
                  CAST('01/01/1980' AS DATE),
                  0.0,
                  produto.tipounidade,
                  valoresproduto.situacaoproduto,--
                  produto.teclaassociadabalanca,
                  produto.diasvalidade,
                  produto.codproduto,
                  subgrupo.codgrupo,
                  produto.incluirbuscarapida,--
                  produto.ippt,
                  estoque.estoquefinal,
                  produto.percentualbaseicms,
                  produto.percentualbcpis,
                  produto.percentualbccofins,
                  COALESCE(tributacaoibpt.aliqnac,0),
                  COALESCE(tributacaoibpt.aliqimp,0),
                  produto.cst_b,
                  produto.codproduto,
                  produto.cst_a,
                  produto.codncm,
                  COALESCE(tributacaoibpt.aliqestadual,0),
                  COALESCE(tributacaoibpt.aliqmunicipal,0),
                  COALESCE(produto.aliquotapis,0),
                  COALESCE(produto.aliquotacofins,0),
                  produto.cstpis,
                  produto.cstcofins,
                  COALESCE(produto.csosn,''),
                  COALESCE(produto.cest,''),
                  Coalesce(valoresproduto.pst,0),
                  Coalesce(valoresproduto.vbcret,0),
                  Coalesce(valoresproduto.vicmsstret,0),
                  Coalesce(produto.PERCREDBCST,0),
                  Coalesce(produto.ALIQUOTAICMSST,0),
                  coalesce(produto.percentualfcp,0),
                   case  when ((produto.cst_b  = '00' and tabadicionalcodinf.cst_b_00 = 'S') or
                   (produto.cst_b  = '10' and tabadicionalcodinf.cst_b_10 = 'S') or
                   (produto.cst_b  = '20' and tabadicionalcodinf.cst_b_20 = 'S') or
                   (produto.cst_b  = '30' and tabadicionalcodinf.cst_b_30 = 'S') or
                   (produto.cst_b  = '40' and tabadicionalcodinf.cst_b_40 = 'S') or
                   (produto.cst_b  = '41' and tabadicionalcodinf.cst_b_41 = 'S') or
                   (produto.cst_b  = '50' and tabadicionalcodinf.cst_b_50 = 'S') or
                   (produto.cst_b  = '51' and tabadicionalcodinf.cst_b_51 = 'S') or
                   (produto.cst_b  = '60' and tabadicionalcodinf.cst_b_60 = 'S') or
                   (produto.cst_b  = '70' and tabadicionalcodinf.cst_b_70 = 'S') or
                   (produto.cst_b  = '90' and tabadicionalcodinf.cst_b_90 = 'S')
                    )  then  tabadicionalcodinf.uf||tabadicionalcodinf.codinfadicional
                   else configuracoes.cbenef end,
                   coalesce(produto.percicmsdesonerado,0)
           FROM   codigobarras,
                  produto LEFT JOIN tributacaoibpt
           ON     (produto.codtributacaoibpt = tributacaoibpt.codtributacaoibpt)
           left join tabadicionalcodinf on tabadicionalcodinf .codtabadicional
                                                    = produto.codtabelaadicional
           and  produto.tipotabelaadiconal = tabadicionalcodinf.tipo,
                  valoresproduto,
                  subgrupo,
                  estoque,
                  ncm_validacao,
                  configuracoes
           WHERE  produto.codproduto        = :in_codproduto
           AND    estoque.codempresa        = :in_codempresa
           and    configuracoes.codempresa            = estoque.codempresa
           and    configuracoes.codempresa            = valoresproduto.codempresa
           AND    estoque.anoestoque        = EXTRACT(YEAR FROM CURRENT_DATE)
           AND    estoque.mesestoque        = EXTRACT(MONTH FROM CURRENT_DATE)
           AND    valoresproduto.codempresa = :in_codempresa
           AND    valoresproduto.precovenda > 0
           AND    produto.codsubgrupo       = subgrupo.codsubgrupo
           AND    codigobarras.codproduto   = produto.codproduto
           AND    produto.codproduto        = valoresproduto.codproduto
           AND    valoresproduto.codproduto = estoque.codproduto
           AND    valoresproduto.codempresa = estoque.codempresa
           AND    valoresproduto.codproduto = codigobarras.codproduto
           AND    valoresproduto.codigobarras = codigobarras.codigobarras      -- Ajuste para requisit #1463
           AND    produto.codncm                      = ncm_validacao.codncm
           AND    produto.codproduto NOT IN (SELECT produtopromocao.codproduto
                                             FROM   promocao,
                                                    produtopromocao,
                                                    empresapromocao
                                             WHERE  CURRENT_DATE               BETWEEN promocao.datainicio AND promocao.datafim
                                             AND    promocao.codpromocao       = produtopromocao.codpromocao
                                             AND    promocao.codpromocao       = empresapromocao.codpromocao
                                             AND    empresapromocao.codempresa = :in_codempresa
                                             AND    promocao.codpromocao        > 0)
           INTO   :out_codigobarras,
                  :out_descricaoreduzida,
                  :out_peso,
                  :out_estoqueminimo,
                  :out_estoquemaximo,
                  :out_unidade,
                  :out_codfornecedor,
                  :out_precovenda,
                  :out_stpdv,
                  :out_aliquotaicms,
                  :out_path_foto,
                  :out_margemobj,
                  :out_customedio,
                  :out_custounitario,
                  :out_dataexportacao,
                  :out_desscricaoreduzida2,
                  :out_retornavel,
                  :out_datainiciopromocao,
                  :out_datafimpromocao,
                  :out_precopromocional,
                  :out_tipounidade,
                  :out_situacaoproduto,  --
                  :out_teclaassociadabalanca,
                  :out_diasvalidade,
                  :out_codproduto,
                  :out_codgrupo,
                  :out_incluirbuscarapida, --
                  :out_ippt,
                  :out_estoqueatual,
                  :out_percentualbaseicms,
                  :out_percentualbcpis,
                  :out_percentualbccofins,
                  :out_aliqnac,
                  :out_aliqimp,
                  :out_cstb,
                  :out_codproduto,
                  :out_csta, 
                  :out_codncm,
                  :out_aliqest,
                  :out_aliqmun,
                  :out_percentualpis,
                  :out_percentualcofins,
                  :out_cstpis,
                  :out_cstcofins,
                  :out_csosn,
                  :out_cest,
                  :out_pst,
                  :out_vbcret,
                  :out_vIcmsSTRet,
                  :out_PercRedBCEfet,
                  :out_ALIQUOTAICMSST,
                  :out_percentualfcp,
                  :out_tabelaadicional,
                  :out_percicmsdesonerado
        DO
        BEGIN
            IF (:out_codigobarras IS NOT NULL) THEN
                SUSPEND;
        END
    END
END^
set term ;^
commit work;
set term ^;
CREATE or alter PROCEDURE PRC_EXPORTAPRODUTOSPDV (
    in_codempresa numeric(9,0),
    in_databaseinicial date,
    in_databasefinal date,
    in_situacaoproduto char(1))
returns (
    out_codigobarras varchar(20),
    out_descricaoreduzida varchar(50),
    out_peso numeric(1,0),
    out_estoqueminimo numeric(15,4),
    out_estoquemaximo numeric(15,4),
    out_unidade varchar(3),
    out_codfornecedor numeric(1,0),
    out_precovenda numeric(15,4),
    out_stpdv numeric(2,0),
    out_aliquotaicms numeric(15,4),
    out_path_foto varchar(100),
    out_margemobj numeric(15,4),
    out_customedio numeric(15,4),
    out_custounitario numeric(15,4),
    out_dataexportacao date,
    out_desscricaoreduzida2 varchar(20),
    out_retornavel char(1),
    out_datainiciopromocao date,
    out_datafimpromocao date,
    out_precopromocional numeric(15,4),
    out_tipounidade char(1),
    out_teclaassociadabalanca char(4),
    out_diasvalidade numeric(4,0),
    out_codproduto numeric(9,0),
    out_codgrupo numeric(9,0),
    out_situacaoproduto char(1),
    out_incluirbuscarapida char(1),
    out_ippt char(1),
    out_estoqueatual numeric(18,4),
    out_percentualbaseicms numeric(15,4),
    out_cstb char(2),
    out_aliqnac numeric(15,4),
    out_aliqimp numeric(15,4),
    out_percentualbcpis float,
    out_percentualbccofins float,
    out_csta char(1),
    out_percentualpis numeric(15,4),
    out_percentualcofins numeric(15,4),
    out_codncm varchar(8),
    out_aliqest numeric(15,4),
    out_aliqmun numeric(15,4),
    out_aliquotapis numeric(15,4),
    out_aliquotacofins numeric(15,4),
    out_cstpis varchar(2),
    out_cstcofins varchar(2),
    out_csosn char(3),
    out_cest varchar(7),
    out_pst numeric(15,4),
    out_vbcret numeric(15,4),
    out_vicmsstret numeric(15,4),
    out_percredbcefet numeric(15,4),
    out_aliquotaicmsst numeric(15,4),
    out_percentualfcp numeric(15,4),
    out_tabelaadicional varchar(10),
    out_percicmsdesonerado numeric(15,4))
as
declare variable v_empromocao integer;
BEGIN
    v_empromocao = 0;

   FOR SELECT
               codigobarras.codigobarras,
               SUBSTRING(produto.descricao FROM 1 FOR 50),
               0,
               valoresproduto.estoqueminimo,
               valoresproduto.estoquemaximo,
               SUBSTRING(valoresproduto.unidademercadoria FROM 1 FOR 3),--produto.unidade,
               0,
               valoresproduto.precovenda,
               produto.situacaotributariatpdv,
               produto.aliquotaicms,
               COALESCE(produto.path_foto,' '),
               0.0,
               0.0,
               --0.0,-- valor que n?o era utilizado e foi tirado e incluido o fatordeconversao
               produto.fatordeconversao,--modificado para colocar o fator de convers?o no sistema
               CURRENT_DATE,
               produto.descricaoreduzida,
               produto.retornavel,
               CAST('01/01/1980' AS DATE),
               CAST('01/01/1980' AS DATE),
               0.0,
               produto.tipounidade,
               valoresproduto.situacaoproduto,--
               produto.teclaassociadabalanca,
               produto.diasvalidade,
               produto.codproduto,
               subgrupo.codgrupo,
               produto.incluirbuscarapida,--
               produto.ippt,
               estoque.estoquefinal,
               produto.percentualbaseicms,
               produto.percentualbcpis,
               produto.percentualbccofins,
               COALESCE(tributacaoibpt.aliqnac,0),
               COALESCE(tributacaoibpt.aliqimp,0),
               produto.cst_b,
               produto.codproduto,
               produto.cst_a,
               produto.codncm,
               COALESCE(tributacaoibpt.aliqestadual,0),
               COALESCE(tributacaoibpt.aliqmunicipal,0),
               produto.aliquotapis,
               produto.aliquotacofins,
               produto.cstpis,
               produto.cstcofins,
               COALESCE(produto.csosn,''),
               COALESCE(produto.cest, ''),
               Coalesce(valoresproduto.pst,0),
               Coalesce(valoresproduto.vbcret,0),
               Coalesce(valoresproduto.vicmsstret,0),
               Coalesce(produto.PERCREDBCST,0),
               Coalesce(produto.ALIQUOTAICMSST,0),
               Coalesce(produto.percentualfcp,0),
                case  when ((produto.cst_b  = '00' and tabadicionalcodinf.cst_b_00 = 'S') or
               (produto.cst_b  = '10' and tabadicionalcodinf.cst_b_10 = 'S') or
               (produto.cst_b  = '20' and tabadicionalcodinf.cst_b_20 = 'S') or
               (produto.cst_b  = '30' and tabadicionalcodinf.cst_b_30 = 'S') or
               (produto.cst_b  = '40' and tabadicionalcodinf.cst_b_40 = 'S') or
               (produto.cst_b  = '41' and tabadicionalcodinf.cst_b_41 = 'S') or
               (produto.cst_b  = '50' and tabadicionalcodinf.cst_b_50 = 'S') or
               (produto.cst_b  = '51' and tabadicionalcodinf.cst_b_51 = 'S') or
               (produto.cst_b  = '60' and tabadicionalcodinf.cst_b_60 = 'S') or
               (produto.cst_b  = '70' and tabadicionalcodinf.cst_b_70 = 'S') or
               (produto.cst_b  = '90' and tabadicionalcodinf.cst_b_90 = 'S')
                )  then  tabadicionalcodinf.uf||tabadicionalcodinf.codinfadicional
                else configuracoes.cbenef end,
               Coalesce(produto.percicmsdesonerado,0)
        FROM   codigobarras,
               produto  
        LEFT JOIN tributacaoibpt
        ON    (produto.codtributacaoibpt = tributacaoibpt.codtributacaoibpt)
        left join tabadicionalcodinf on tabadicionalcodinf .codtabadicional
                                                    = produto.codtabelaadicional
                     and  produto.tipotabelaadiconal = tabadicionalcodinf.tipo,
               valoresproduto,
               subgrupo,
               estoque,
               ncm_validacao,
               configuracoes
        WHERE  codigobarras.codproduto             = produto.codproduto
        and    configuracoes.codempresa            = estoque.codempresa
        and    configuracoes.codempresa            = valoresproduto.codempresa
        AND    produto.codncm                      = ncm_validacao.codncm
        AND    produto.codsubgrupo                 = subgrupo.codsubgrupo
        AND    produto.codproduto                  = valoresproduto.codproduto
        AND    produto.codproduto                  = estoque.codproduto
        AND    estoque.codproduto                  = codigobarras.codproduto
        AND    estoque.codempresa                  = :in_codempresa
        AND    estoque.anoestoque                  = EXTRACT(YEAR FROM CURRENT_DATE)
        AND    estoque.mesestoque                  = EXTRACT(MONTH FROM CURRENT_DATE)
        AND    estoque.codproduto                  = valoresproduto.codproduto
        AND    valoresproduto.codproduto           = codigobarras.codproduto
        AND    valoresproduto.codigobarras         = codigobarras.codigobarras
        AND    valoresproduto.codempresa           = estoque.codempresa
        AND    valoresproduto.precovenda           > 0
        AND    (valoresproduto.ultimaalteracaovenda BETWEEN :in_databaseinicial  AND :in_databasefinal
        OR     PRODUTO.dataalteracaocadastro       BETWEEN :in_databaseinicial  AND :in_databasefinal )
        AND    valoresproduto.situacaoproduto      LIKE :in_situacaoproduto
        --AND    valoresproduto.unidadeprincipal     = 'S'
        AND   ((produto.codtipoitem = 0) OR (produto.codtipoitem = 4) OR (produto.codtipoitem = 9)) -- alterado por vin?cius no dia 19/04/2010
        AND produto.codproduto NOT in (SELECT produtopromocao.codproduto
                        FROM   promocao,
                               produto,
                               produtopromocao,
                               empresapromocao
                        WHERE CURRENT_DATE               BETWEEN promocao.datainicio AND promocao.datafim
                        AND   promocao.codpromocao       = produtopromocao.codpromocao
                        AND   produto.codproduto         = produtopromocao.codproduto
                        AND   promocao.codpromocao       = empresapromocao.codpromocao
                        AND   empresapromocao.codempresa = :in_codempresa)    
        INTO :out_codigobarras,
             :out_descricaoreduzida,
             :out_peso,
             :out_estoqueminimo,
             :out_estoquemaximo,
             :out_unidade,
             :out_codfornecedor,
             :out_precovenda,
             :out_stpdv,
             :out_aliquotaicms,
             :out_path_foto,
             :out_margemobj,
             :out_customedio,
             :out_custounitario,
             :out_dataexportacao,
             :out_desscricaoreduzida2,
             :out_retornavel,
             :out_datainiciopromocao,
             :out_datafimpromocao,
             :out_precopromocional,
             :out_tipounidade,
             :out_situacaoproduto,  --
             :out_teclaassociadabalanca,
             :out_diasvalidade,
             :out_codproduto,
             :out_codgrupo,
             :out_incluirbuscarapida, --
             :out_ippt,
             :out_estoqueatual,
             :out_percentualbaseicms,
             :out_percentualbcpis,
             :out_percentualbccofins,
             :out_aliqnac,
             :out_aliqimp,
             :out_cstb,
             :out_codproduto,
             :out_csta,
             :out_codncm,
             :out_aliqest,
             :out_aliqmun,
             :out_percentualpis,
             :out_percentualcofins,
             :out_cstpis,
             :out_cstcofins,
             :out_csosn,
             :out_cest,
             :out_pst,
             :out_vbcret,
             :out_vIcmsSTRet,
             :out_PercRedBCEfet,
             :out_ALIQUOTAICMSST,
             :out_percentualfcp,
             :out_tabelaadicional,
             :out_percicmsdesonerado
        DO
        BEGIN
            SELECT COUNT (*)
            FROM  promocao,
                  produtopromocao,
                  empresapromocao
            WHERE CURRENT_DATE                     BETWEEN promocao.datainicio AND promocao.datafim
            AND   produtopromocao.codproduto       = :out_codproduto
            AND   promocao.codpromocao             = produtopromocao.codpromocao
            AND   promocao.codpromocao             = empresapromocao.codpromocao
            AND   empresapromocao.codempresa       = :in_codempresa
            AND   produtopromocao.precopromocional > 0
            GROUP BY produtopromocao.codproduto
            INTO  :v_empromocao;

            IF (:v_empromocao = 0) THEN
            BEGIN
                SUSPEND;
                v_empromocao = 0;
            END
       END

END^
set term ;^
commit work;
set term ^;
CREATE or alter PROCEDURE PRC_EXPORTAPRODUTOSPROMOCAOPDV (
    in_codempresa numeric(9,0),
    in_situacaoproduto char(1))
returns (
    out_codigobarras varchar(20),
    out_descricaoreduzida varchar(50),
    out_peso float,
    out_estoqueminimo float,
    out_estoquemaximo float,
    out_unidade varchar(3),
    out_codfornecedor float,
    out_precovenda float,
    out_stpdv float,
    out_aliquotaicms float,
    out_path_foto varchar(100),
    out_margemobj float,
    out_customedio float,
    out_custounitario float,
    out_dataexportacao date,
    out_desscricaoreduzida2 varchar(20),
    out_retornavel char(1),
    out_datainiciopromocao date,
    out_datafimpromocao date,
    out_precopromocional float,
    out_codpromocao float,
    out_tipounidade char(1),
    out_teclaassociadabalanca char(4),
    out_diasvalidade float,
    out_codproduto float,
    out_codgrupo float,
    out_situacaoproduto char(1),
    out_incluirbuscarapida char(1),
    out_ippt char(1),
    out_estoqueatual float,
    out_percentualbaseicms numeric(15,4),
    out_cstb char(2),
    out_aliqnac float,
    out_aliqimp float,
    out_percentualbcpis float,
    out_percentualbccofins float,
    out_csta char(1),
    out_percentualpis numeric(15,4),
    out_percentualcofins numeric(15,4),
    out_codncm varchar(8),
    out_aliqest numeric(15,4),
    out_aliqmun numeric(15,4),
    out_aliquotapis numeric(15,4),
    out_aliquotacofins numeric(15,4),
    out_cstpis varchar(2),
    out_cstcofins varchar(2),
    out_csosn char(3),
    out_cest varchar(7),
    out_pst numeric(15,4),
    out_vbcret numeric(15,4),
    out_vicmsstret numeric(15,4),
    out_percredbcefet numeric(15,4),
    out_aliquotaicmsst numeric(15,4),
    out_percentualfcp numeric(15,4),
    out_tabelaadicional varchar(10),
    out_percicmsdesonerado numeric(15,4))
as
BEGIN

     FOR SELECT codigobarras.codigobarras,
                SUBSTRING(produto.descricao FROM 1 FOR 50),
                0,
                valoresproduto.estoqueminimo,
                valoresproduto.estoquemaximo,
                SUBSTRING(produto.unidade FROM 1 FOR 3),
                0,
                valoresproduto.precovenda,
                produto.situacaotributariatpdv,
                produto.aliquotaicms,
                COALESCE(produto.path_foto,' ') AS path_foto,
                0.0,
                0.0,
                --0.0, retirado para passar o fatordeconverssao
                produto.fatordeconversao,-- modifica??o feita por giucimar vieira
                CURRENT_DATE,
                produto.descricaoreduzida,
                produto.retornavel,
                promocao.datainicio,
                promocao.datafim,
                produtopromocao.precopromocional,
                promocao.codpromocao,
                produto.tipounidade,
                valoresproduto.situacaoproduto,--
                produto.teclaassociadabalanca,
                produto.diasvalidade,
                produto.codproduto,
                subgrupo.codgrupo,
                produto.incluirbuscarapida,--
                produto.ippt,
                estoque.estoquefinal,
                produto.percentualbaseicms,
                produto.percentualbcpis,
                produto.percentualbccofins,
                COALESCE(tributacaoibpt.aliqnac,0),
                COALESCE(tributacaoibpt.aliqimp,0),
                produto.cst_b,
                produto.codproduto,
                produto.cst_a,
                produto.codncm,
                COALESCE(tributacaoibpt.aliqestadual,0),
                COALESCE(tributacaoibpt.aliqmunicipal,0),
                produto.aliquotapis,
                produto.aliquotacofins,
                produto.cstpis,
                produto.cstcofins,
                COALESCE(produto.csosn,''),
                COALESCE(produto.cest, ''),
                Coalesce(valoresproduto.pst,0),
                Coalesce(valoresproduto.vbcret,0),
                Coalesce(valoresproduto.vicmsstret,0),
                Coalesce(produto.PERCREDBCST,0),
                Coalesce(produto.ALIQUOTAICMSST,0),
                Coalesce(produto.percentualfcp,0),
                case  when ((produto.cst_b  = '00' and tabadicionalcodinf.cst_b_00 = 'S') or
               (produto.cst_b  = '10' and tabadicionalcodinf.cst_b_10 = 'S') or
               (produto.cst_b  = '20' and tabadicionalcodinf.cst_b_20 = 'S') or
               (produto.cst_b  = '30' and tabadicionalcodinf.cst_b_30 = 'S') or
               (produto.cst_b  = '40' and tabadicionalcodinf.cst_b_40 = 'S') or
               (produto.cst_b  = '41' and tabadicionalcodinf.cst_b_41 = 'S') or
               (produto.cst_b  = '50' and tabadicionalcodinf.cst_b_50 = 'S') or
               (produto.cst_b  = '51' and tabadicionalcodinf.cst_b_51 = 'S') or
               (produto.cst_b  = '60' and tabadicionalcodinf.cst_b_60 = 'S') or
               (produto.cst_b  = '70' and tabadicionalcodinf.cst_b_70 = 'S') or
               (produto.cst_b  = '90' and tabadicionalcodinf.cst_b_90 = 'S')
                )  then  tabadicionalcodinf.uf||tabadicionalcodinf.codinfadicional
                else configuracoes.cbenef end,
                coalesce(produto.percicmsdesonerado,0)
         FROM   codigobarras,
                produto LEFT JOIN tributacaoibpt
         ON    (produto.codtributacaoibpt = tributacaoibpt.codtributacaoibpt)
                left join tabadicionalcodinf on tabadicionalcodinf .codtabadicional
                                                    = produto.codtabelaadicional
                and  produto.tipotabelaadiconal = tabadicionalcodinf.tipo,
                valoresproduto,
                promocao,
                produtopromocao,
                empresapromocao,
                subgrupo,
                estoque,
                ncm_validacao,
               configuracoes
         WHERE  codigobarras.codproduto             = produto.codproduto
         and    configuracoes.codempresa            = estoque.codempresa
         and    configuracoes.codempresa            = valoresproduto.codempresa
         AND    produto.codsubgrupo              = subgrupo.codsubgrupo
         AND    produto.codproduto               = valoresproduto.codproduto
         AND    produto.codproduto               = estoque.codproduto
         AND    produto.codproduto               = produtopromocao.codproduto
         AND    estoque.codempresa               = :in_codempresa
         AND    estoque.anoestoque               = EXTRACT(YEAR FROM CURRENT_DATE)
         AND    estoque.mesestoque               = EXTRACT(MONTH FROM CURRENT_DATE)
         AND    codigobarras.codproduto          = valoresproduto.codproduto
         AND    valoresproduto.codigobarras      = codigobarras.codigobarras
         AND    valoresproduto.codempresa        = estoque.codempresa
         AND    CURRENT_DATE                     BETWEEN promocao.datainicio AND promocao.datafim
         AND    promocao.codpromocao             = empresapromocao.codpromocao
         AND    promocao.codpromocao             = produtopromocao.codpromocao
         AND    empresapromocao.codempresa       = estoque.codempresa
         AND    produtopromocao.precopromocional > 0
         AND    valoresproduto.situacaoproduto   LIKE :in_situacaoproduto
         AND    ((produto.codtipoitem = 0) OR (produto.codtipoitem = 4) OR (produto.codtipoitem = 9)) -- alterado por vin?cius no dia 19/04/2010
 --       and  (produtopromocao.codpromocao||produtopromocao.codproduto) in
         AND    produto.codncm                      = ncm_validacao.codncm
         AND EXISTS (SELECT DISTINCT MAX(produtopromocao.codpromocao)||produtopromocao.codproduto
                     FROM promocao,
                          produtopromocao,
                          empresapromocao
                     WHERE CURRENT_DATE BETWEEN promocao.datainicio AND promocao.datafim
                     AND  promocao.codpromocao = produtopromocao.codpromocao
                     AND  promocao.codpromocao = empresapromocao.codpromocao
                     AND  empresapromocao.codempresa = :in_codempresa
                     GROUP BY produtopromocao.codproduto)
         INTO   :out_codigobarras,
                :out_descricaoreduzida,
                :out_peso,
                :out_estoqueminimo,
                :out_estoquemaximo,
                :out_unidade,
                :out_codfornecedor,
                :out_precovenda,
                :out_stpdv,
                :out_aliquotaicms,
                :out_path_foto,
                :out_margemobj,
                :out_customedio,
                :out_custounitario,
                :out_dataexportacao,
                :out_desscricaoreduzida2,
                :out_retornavel,
                :out_datainiciopromocao,
                :out_datafimpromocao,
                :out_precopromocional,
                :out_codpromocao,
                :out_tipounidade,
                :out_situacaoproduto, --
                :out_teclaassociadabalanca,
                :out_diasvalidade,
                :out_codproduto,
                :out_codgrupo,
                :out_incluirbuscarapida, --
                :out_ippt,
                :out_estoqueatual,
                :out_percentualbaseicms,
                :out_percentualbcpis,
                :out_percentualbccofins,
                :out_aliqnac,
                :out_aliqimp,
                :out_cstb,
                :out_codproduto,
                :out_csta,
                :out_codncm,
                :out_aliqest,
                :out_aliqmun,
                :out_percentualpis,
                :out_percentualcofins,
                :out_cstpis,
                :out_cstcofins,
                :out_csosn,
                :out_cest,
                :out_pst,
                :out_vbcret,
                :out_vIcmsSTRet,
                :out_PercRedBCEfet,
                :out_ALIQUOTAICMSST,
                :out_percentualfcp,
                :out_tabelaadicional,
                :out_percicmsdesonerado
     DO
        SUSPEND;
 END^
set term ;^
commit work;
set term ^;
CREATE or alter PROCEDURE PRC_RETORNAITENSNFE (
    in_codempresa integer,
    in_numeronota integer,
    in_serienota varchar(3),
    in_numeroitem integer)
returns (
    out_codproduto integer,
    out_numeroitemnota integer,
    out_codigobarras varchar(20),
    out_descricao varchar(100),
    out_codigoncm varchar(8),
    out_extipi varchar(3),
    out_genero integer,
    out_cfop integer,
    out_unidadecomercial char(3),
    out_qtdcomercial numeric(15,4),
    out_valorunitariovenda numeric(15,4),
    out_valortotalbruto numeric(15,2),
    out_codigobarrastributavel varchar(20),
    out_unidadetributavel varchar(3),
    out_qtdtributavel numeric(15,4),
    out_valorunitariotributacao numeric(15,4),
    out_valorfrete numeric(15,2),
    out_valorseguro numeric(15,2),
    out_valordesconto numeric(15,2),
    out_cst_a char(1),
    out_cst_b char(2),
    out_modalidadebcdoicms integer,
    out_valorbaseicms numeric(15,2),
    out_percentualicms numeric(5,2),
    out_valoricms numeric(15,2),
    out_modalidadebcdoicms_st integer,
    out_percmargemvaloraddicms_st numeric(5,2),
    out_percredbcdoicms_st numeric(5,2),
    out_valorbcdoicms_st numeric(15,2),
    out_aliquotaicms_st numeric(5,2),
    out_valoricms_st numeric(15,2),
    out_percredbasecalculo numeric(5,2),
    out_cstpis char(2),
    out_percentualbcpis numeric(15,2),
    out_valorbcpis numeric(15,2),
    out_valorpis numeric(15,2),
    out_qtdbcprodutopis numeric(16,4),
    out_aliquotapis numeric(15,4),
    out_cstcofins char(2),
    out_percentualbccofins numeric(5,2),
    out_valorbccofins numeric(15,2),
    out_valorcofins numeric(15,2),
    out_qtdbcprodutocofins numeric(16,4),
    out_aliquotacofins numeric(15,4),
    out_situacaotributariatpdv integer,
    out_codlistaservicos integer,
    out_valorbaseissqn numeric(15,2),
    out_valorissqn numeric(15,2),
    out_aliquotaissqn numeric(15,2),
    out_percentualbaseissqn numeric(15,2),
    out_valoripi numeric(15,2),
    out_percentualipi numeric(15,2),
    out_codenquadramentoipi integer,
    out_cstipi char(2),
    out_valorbaseipi numeric(15,2),
    out_quantidadeunidadetribipi numeric(15,2),
    out_valorunidadetribipi numeric(15,2),
    out_valoroutrasdespesas numeric(15,2),
    out_csosn varchar(3),
    out_aliquotacreditoicmssn numeric(15,4),
    out_valorcreditoicmssn numeric(15,4),
    out_cest varchar(7),
    out_vbcfcp numeric(15,4),
    out_pfcp numeric(15,4),
    out_vfcp numeric(15,4),
    out_vbcfcpst numeric(15,4),
    out_pfcpst numeric(15,4),
    out_vfcpst numeric(15,4),
    out_pst numeric(15,4),
    out_vbcfcpstret numeric(15,4),
    out_pfcpstret numeric(15,4),
    out_vfcpstret numeric(15,4),
    out_valoricmsdeson numeric(15,4),
    out_percredbcefet numeric(15,4),
    out_valorbcefet numeric(15,4),
    out_percicmsefet numeric(15,4),
    out_valoricmsefet numeric(15,4),
    out_valorbcstret numeric(15,4),
    out_valoricmsstret numeric(15,4),
    out_valoricmssubstituto numeric(15,4),
    out_tabelaadicional varchar(10),
    out_percicmsdesonerado numeric(15,2))
as
declare variable v_produtoinsidepiscofins char(1);
declare variable v_valoripi numeric(15,2);
declare variable v_percentualipi numeric(15,2);
declare variable v_valorbaseipi numeric(15,2);
BEGIN
     SELECT FIRST 1
            itemnfe.codproduto,
            itemnfe.itemnfe,
            codigobarras.codigobarras,
            produto.descricao,
            produto.codncm,
            NULL,-- EX_TIPI
            NULL,--genero,
            itemnfe.cfop,
            SUBSTRING(produto.unidade FROM 1 FOR 3),
            (itemnfe.quantidade * itemnfe.qtdcaixas),--quantidade comercial
            itemnfe.valorunitario,
            itemnfe.valortotalitem, -- valor total bruto
            codigobarras.codigobarras,
            SUBSTRING(produto.unidade FROM 1 FOR 3), --unidade tributavel,
            (itemnfe.quantidade * itemnfe.qtdcaixas), --quantidade tributavel,
            itemnfe.valorunitario, -- valor unitario  de tributa?ao
            itemnfe.valorfrete,
            itemnfe.valorseguro,--  valor total do seguro
            itemnfe.valordesconto,
            itemnfe.valorpis,
            itemnfe.valorcofins,
            itemnfe.cst_a,
            itemnfe.cst_b,
            NULL, -- modBC = modalidade de determnia??o da BC do ICMS , pg 101,campo 168 do manual de integra??o
            itemnfe.valorbaseicms,
            itemnfe.aliquotaicms,
            itemnfe.valoricms,
            4, -- modBCST = Modalidade de determina??o da BC do ICMS ST, pg 102 (130 na versao 4.01) ,campo 179(N18) do manual de integra??o
            itemnfe.mva, -- pMVAST  = Percentual da margem de valor adicionado do ICMS ST
            0, -- pRedBCST= Percentual da redu??o de BC do ICMS ST
            itemnfe.valorbaseicmsst, -- vBCST   = Valor da BC do ICMS ST
            itemnfe.aliquotaicmsst, -- pICMSST = Aliquota do imposto do ICMS ST
            itemnfe.valoricmsst, -- vICMSST = Valor do ICMS ST
            CASE itemnfe.cst_b  -- quando for CST B 20 (reducao de BC) ou 90 (outras) o perc de redu??o ? igual perc da base de calculo
                 WHEN '20' THEN itemnfe.percentualbaseicms
                 WHEN '90' THEN itemnfe.percentualbaseicms
                 ELSE 0
            END AS PercReducaoBC, -- pRedBC  = Percentual da redu??o de BC
            produto.incidepisconfins,
            itemnfe.cstpis, -- PIS
            itemnfe.percentualbcpis,
            itemnfe.bcpis,
            itemnfe.aliquotapis,
            itemnfe.valorpis,   -- FIM PIS
            itemnfe.cstcofins,  --COFINS
            itemnfe.percentualbccofins,
            itemnfe.bccofins,
            itemnfe.aliquotacofins,
            itemnfe.valorcofins, --FIM COFINS
            produto.situacaotributariatpdv,
            produto.codclassificacaoservico,
            itemnfe.valorbaseissqn,
            itemnfe.valorissqn,
            itemnfe.aliquotaissqn,
            itemnfe.percentualbaseissqn,
            itemnfe.valoripi,
            itemnfe.percentualipi,
            itemnfe.valorbaseipi,
            itemnfe.cst_ipi,
            itemnfe.valoroutrasdespesas,
            itemnfe.csosn,
            itemnfe.aliquotacreditoicmssn,
            itemnfe.valorcreditoicmssn,
            produto.cest,
            itemnfe.vBCFCP,
            itemnfe.pFCP,
            itemnfe.vFCP,
            itemnfe.vBCFCPST,
            itemnfe.pFCPST,
            itemnfe.vFCPST,
            itemnfe.pST,
            itemnfe.vBCFCPSTRet,
            itemnfe.pFCPSTRet,
            itemnfe.vFCPSTRet,
            itemnfe.valoricmsdeson,
            itemnfe.PERCREDBCEFET,
            itemnfe.VALORBCEFET,
            itemnfe.PERCICMSEFET,
            itemnfe.VALORICMSEFET,
            itemnfe.VALORBCSTRET,
            itemnfe.VALORICMSSTRET,
            itemnfe.valoricmssubstituto,
            case  when ((itemnfe.cst_b  = '00' and tabadicionalcodinf.cst_b_00 = 'S') or
            (itemnfe.cst_b  = '10' and tabadicionalcodinf.cst_b_10 = 'S') or
            (itemnfe.cst_b  = '20' and tabadicionalcodinf.cst_b_20 = 'S') or
            (itemnfe.cst_b  = '30' and tabadicionalcodinf.cst_b_30 = 'S') or
            (itemnfe.cst_b  = '40' and tabadicionalcodinf.cst_b_40 = 'S') or
            (itemnfe.cst_b  = '41' and tabadicionalcodinf.cst_b_41 = 'S') or
            (itemnfe.cst_b  = '50' and tabadicionalcodinf.cst_b_50 = 'S') or
            (itemnfe.cst_b  = '51' and tabadicionalcodinf.cst_b_51 = 'S') or
            (itemnfe.cst_b  = '60' and tabadicionalcodinf.cst_b_60 = 'S') or
            (itemnfe.cst_b  = '70' and tabadicionalcodinf.cst_b_70 = 'S') or
            (itemnfe.cst_b  = '90' and tabadicionalcodinf.cst_b_90 = 'S')
            )  then  tabadicionalcodinf.uf||tabadicionalcodinf.codinfadicional
            else configuracoes.cbenef end,
            coalesce(produto.percicmsdesonerado,0)
     FROM   itemnfe,
            produto left join tabadicionalcodinf on tabadicionalcodinf .codtabadicional
                                                    = produto.codtabelaadicional
                    and  produto.tipotabelaadiconal = tabadicionalcodinf.tipo,
            codigobarras,
            configuracoes
     WHERE  itemnfe.numeronfe        = :in_numeronota
     and    configuracoes.codempresa =  itemnfe.codempresa
     AND    itemnfe.serienfe       = :in_serienota
     AND    itemnfe.codempresa     = :in_codempresa
     AND    itemnfe.itemnfe        > :in_numeroitem
     AND    itemnfe.codproduto     = produto.codproduto
     AND    itemnfe.codproduto     = codigobarras.codproduto
     AND    codigobarras.principal = 'S'
     ORDER BY itemnfe.itemnfe
     INTO   :out_codproduto,
            :out_numeroitemnota,
            :out_codigobarras,
            :out_descricao,
            :out_codigoncm,
            :out_extipi,
            :out_genero,
            :out_cfop,
            :out_unidadecomercial,
            :out_qtdcomercial,
            :out_valorunitariovenda,
            :out_valortotalbruto,
            :out_codigobarrastributavel,
            :out_unidadetributavel,
            :out_qtdtributavel,
            :out_valorunitariotributacao,
            :out_valorfrete,
            :out_valorseguro,
            :out_valordesconto,
            :out_valorpis,
            :out_valorcofins,
            :out_cst_a,
            :out_cst_b,
            :out_modalidadebcdoicms,
            :out_valorbaseicms,
            :out_percentualicms,
            :out_valoricms,
            :out_modalidadebcdoicms_st,
            :out_percmargemvaloraddicms_st,
            :out_percredbcdoicms_st,
            :out_valorbcdoicms_st,
            :out_aliquotaicms_st,
            :out_valoricms_st,
            :out_percredbasecalculo,
            :v_produtoinsidepiscofins,
            :out_cstpis, -- pis
            :out_percentualbcpis,
            :out_valorbcpis,
            :out_aliquotapis,
            :out_valorpis,   -- fim pis
            :out_cstcofins,  --cofins
            :out_percentualbccofins,
            :out_valorbccofins,
            :out_aliquotacofins,
            :out_valorcofins, --fim cofins
            :out_situacaotributariatpdv,
            :out_codlistaservicos,
            :out_valorbaseissqn,
            :out_valorissqn,
            :out_aliquotaissqn,
            :out_percentualbaseissqn,
            :v_valoripi,
            :v_percentualipi,
            :v_valorbaseipi,
            :out_cstipi,
            :out_valoroutrasdespesas,
            :out_csosn,
            :out_aliquotacreditoicmssn,
            :out_valorcreditoicmssn,
            :out_cest,
            :out_vBCFCP,
            :out_pFCP,
            :out_vFCP,
            :out_vBCFCPST,
            :out_pFCPST,
            :out_vFCPST,
            :out_pST,
            :out_vBCFCPSTRet,
            :out_pFCPSTRet,
            :out_vFCPSTRet,
            :out_valoricmsdeson,
            :out_PercRedBCEfet,
            :out_ValorBCEfet,
            :out_PercIcmsEfet,
            :out_ValorIcmsEfet,
            :out_ValorBCSTRet,
            :out_ValorIcmsSTRet,
            :out_valoricmssubstituto,
            :out_tabelaadicional,
            :out_percicmsdesonerado;

 
     ------------
     -- IPI --
     ------------
     IF (:out_cstipi IN ('00','49','50','99')) THEN
     BEGIN
         out_valoripi      = :v_valoripi;
         out_valorbaseipi  = :v_valorbaseipi;
         out_percentualipi = :v_percentualipi;
         ------------------------------------------------------------------------------------------
         -- Segundo manual da NF-e, ser? criada uma tabela com os c?digos dos enquadramentos     --
         -- legais do IPI. Por enquanto informar 999                                             --
         ------------------------------------------------------------------------------------------
         out_codenquadramentoipi = '999';
     END
     ELSE IF (:out_cstipi IN ('01','02','03','04','05','51','52','53','54','55')) THEN
     BEGIN
         ---------------------------------------------------------------------------
         -- CSTs n?o tributadas                                                    --
         ---------------------------------------------------------------------------
         ------------------------------------------------------------------------------------------
         -- Segundo manual da NF-e, ser? criada uma tabela com os c?digos dos enquadramentos     --
         -- legais do IPI. Por enquanto informar 999                                             --
         ------------------------------------------------------------------------------------------
         out_codenquadramentoipi = '999';
     END
     SUSPEND;
END^
set term ;^
commit work;